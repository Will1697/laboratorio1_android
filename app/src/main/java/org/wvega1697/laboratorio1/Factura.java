package org.wvega1697.laboratorio1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by WVega1697 on 12/05/2016.
 */
public class Factura extends AppCompatActivity {

    private TextView txtPedido = null;
    private TextView txtTotal = null;
    private Button btnRegresar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura);
        txtPedido = (TextView) findViewById(R.id.txt_Pedido);
        txtTotal = (TextView) findViewById(R.id.txt_Total);
        Bundle bundle = getIntent().getExtras();

        txtPedido.setText(""+ bundle.getString("Pedido"));
        btnRegresar = (Button) findViewById(R.id.btn_regresar);
        Double total = Double.parseDouble(bundle.getString("Pedido"))* 7.50;
        txtTotal.setText(""+total);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Factura.this, MainActivity.class));
            }
        });
    }
}
